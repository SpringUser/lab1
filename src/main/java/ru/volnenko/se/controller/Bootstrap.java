package ru.volnenko.se.controller;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.error.CommandCorruptException;

import java.util.*;

/**
 * @author Denis Volnenko
 */
@Component
public class Bootstrap implements InitializingBean {

    @Autowired
    private List<AbstractCommand> commandList;

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Scanner scanner = new Scanner(System.in);

    public void registry(List<AbstractCommand> list) {
        for (AbstractCommand command : list) {
            final String cliCommand = command.command();
            final String cliDescription = command.description();
            if (cliCommand == null || cliCommand.isEmpty()) throw new CommandCorruptException();
            if (cliDescription == null || cliDescription.isEmpty()) throw new CommandCorruptException();
            command.setBootstrap(this);
            commands.put(cliCommand, command);
        }
    }

    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    public List<AbstractCommand> getListCommand() {
        return new ArrayList<>(commands.values());
    }

    public String nextLine() {
        return scanner.nextLine();
    }

    public Integer nextInteger() {
        final String value = nextLine();
        if (value == null || value.isEmpty()) return null;
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        registry(commandList);
    }
}
